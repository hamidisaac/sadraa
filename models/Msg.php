<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "msg".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $ID
 * @property mixed $From
 * @property mixed $To
 * @property mixed $Date
 * @property mixed $Body
 * @property mixed $pin
 * @property mixed $branchId
 * @property mixed $customerId
 * @property mixed $plan
 * @property mixed $status
 * @property mixed $code
 * @property mixed $msg
 * @property mixed $time
 */
class Msg extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        $plans = \Yii::$app->mohsendb->getCollection('plans');
        $activePlan = $plans->find(["s"=>1])->toArray();
        $planId = $activePlan[0]['planId'];
        return ['mohsen', 'msg'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'ID',
            'From',
            'To',
            'Date',
            'Body',
            'pin',
            'branchId',
            'customerId',
            'plan',
            'status',
            'code',
            'msg',
            'time',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'From', 'To', 'Date', 'Body', 'pin', 'branchId', 'customerId', 'plan', 'status', 'code', 'msg', 'time'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ردیف',
            'ID' => 'شناسه',
            'From' => 'فرستنده',
            'To' => 'گیرنده',
            'Date' => 'زمان',
            'Body' => 'متن',
            'pin' => 'پین کد',
            'branchId' => 'شعبه',
            'customerId' => 'کد پیامک',
            'plan' => 'دوره',
            'status' => 'وضعیت',
            'code' => 'کد قرعه کشی',
            'msg' => 'پاسخ',
            'time' => 'زمان',
        ];
    }
}
