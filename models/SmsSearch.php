<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sms;

/**
 * SmsSearch represents the model behind the search form of `app\models\Sms`.
 */
class SmsSearch extends Sms
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['_id', 'id', 's', 'd', 'b', 'c', 'l', 'p', 'f', 'm', 'n', 'o', 'r', 't'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 's', $this->s])
            ->andFilterWhere(['like', 'd', $this->d])
            ->andFilterWhere(['like', 'b', $this->b])
            ->andFilterWhere(['like', 'c', $this->c])
            ->andFilterWhere(['like', 'l', $this->l])
            ->andFilterWhere(['like', 'p', $this->p])
            ->andFilterWhere(['like', 'f', $this->f])
            ->andFilterWhere(['like', 'm', $this->m])
            ->andFilterWhere(['like', 'n', $this->n])
            ->andFilterWhere(['like', 'o', $this->o])
            ->andFilterWhere(['like', 'r', $this->r])
            ->andFilterWhere(['like', 't', $this->t]);

        return $dataProvider;
    }
}
