<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "comments".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $s
 * @property mixed $f
 * @property mixed $m
 * @property mixed $b
 * @property mixed $t
 */
class Comments extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return ['sadraa', 'comments'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            's',
            'f',
            'm',
            'b',
            't',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['s', 'f', 'm', 'b', 't'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ردیف',
            's' => 'وضعیت',
            'f' => 'سرشماره',
            'm' => 'موبایل',
            'b' => 'متن',
            't' => 'زمان',
        ];
    }
}
