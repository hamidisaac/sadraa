<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "sms".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $id
 * @property mixed $s
 * @property mixed $b
 * @property mixed $c
 * @property mixed $l
 * @property mixed $p
 * @property mixed $f
 * @property mixed $m
 * @property mixed $n
 * @property mixed $o
 * @property mixed $r
 * @property mixed $t
 */
class Sms extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        $plans = \Yii::$app->mongodb->getCollection('plans');
        $activePlan = $plans->find(["s"=>1])->toArray();
        $planId = $activePlan[0]['planId'];
        return ['sadraa', 'sms'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            's',
            'd',
            'b',
            'c',
            'l',
            'p',
            'f',
            'm',
            'n',
            'o',
            'r',
            't',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 's', 'd', 'b', 'c', 'l', 'p', 'f', 'm', 'n', 'o', 'r', 't'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ردیف',
            'id' => 'شناسه',
            's' => 'وضعیت',
            'd' => 'دوره',
            'b' => 'متن',
            'c' => 'گروه',
            'l' => 'کالکشن',
            'p' => 'پین کد',
            'f' => 'به',
            'm' => 'موبایل',
            'n' => 'از',
            'o' => 'پاسخ',
            'r' => 'نتیجه',
            't' => 'زمان',
        ];
    }
}
