<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "cat".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $p
 * @property mixed $s
 */
class Cat extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return ['sadraa', 'cat-07'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'p',
            's',
            'm',
            'l',
            't',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p', 's', 'm', 'l', 't'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ردیف',
            'p' => 'پین کد',
            's' => 'سریال',
            'm' => 'موبایل',
            'l' => 'دوره',
            't' => 'زمان',
        ];
    }
}
