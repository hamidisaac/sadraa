<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cat;

/**
 * CatSearch represents the model behind the search form of `app\models\Cat`.
 */
class CatSearch extends Cat
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p', 's', 'm', 'l', 't'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'p', $this->p])
            ->andFilterWhere(['like', 's', $this->s])
            ->andFilterWhere(['like', 'm', $this->m])
            ->andFilterWhere(['like', 'l', $this->l]);

        return $dataProvider;
    }
}
