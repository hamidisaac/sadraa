var request = require('request');
var config = require("../config");
var soap = require('soap');
var amqp = require('amqplib/callback_api');
var cron = require('node-cron');
var MongoClient = require("mongodb").MongoClient;
var telegram_msg = '';
process.env.TZ = 'Asia/Tehran';
var query = {};

MongoClient.connect(config.dbs.mongo_subs, function (err, subdb) {
    if (err) throw err;
    MongoClient.connect(config.dbs.mongo_sends, function (err, db) {
        if (err) throw err;
        MongoClient.connect(config.dbs.mongo_panel, function (err, paneldb) {
            if (err) throw err;
            amqp.connect('amqp://localhost', function (err, conn) {
                conn.createChannel(function (err, ch) {
                    var mtq = 'rightel_mt';
                    ch.assertQueue(mtq, {durable: true, maxPriority: 10});
                    cron.schedule('0 * * * * *', function () {
                        // check send
                        db.collection("sends").findOne({
                            op: 'rightel',
                            t: {
                                $lt: new Date()
                            },
                            s: '0'
                        }, function (err, send) {
                            console.log('send');
                            console.log(send);
                            if (err) throw err;
                            if (send) {
                                paneldb.collection("services").findOne({
                                    service_key: send.sid,
                                    s: '1'
                                }, function (err, service) {
                                    if (err) throw err;
                                    //console.log(service);
                                    query = {};
                                    if(send.active_users != '-1')
                                        query = {s: parseInt(send.active_users)};
                                    subdb.collection(send.list).count(query, function (err, total) {
                                        if (err) throw err;
                                        console.log('total : ', total);
                                        if (service) {
                                            telegram_msg = 'ارسال شروع شد' + '\n' +
                                                'عنوان : ' + service.title + '\n' +
                                                'اپراتور : ' + send.op + '\n' +
                                                'سرویس : ' + send.sid + '\n' +
                                                'ارسال : ' + send.send_id + '\n' +
                                                'تعداد : ' + total + '\n' +
                                                'شماره : ' + send.number + '\n' +
                                                'لیست : ' + send.list + '\n' +
                                                'شارژ : ' + send.charging + '\n' +
                                                'اولویت : ' + send.priority + '\n' +
                                                'کاربران فعال : ' + send.active_users + '\n' +
                                                'پیام : ' + send.msg
                                                +'\n'+'****************';
                                            // var url = 'https://api.telegram.org/bot428825362:AAEgSDWWqKSvk3ao1Ag5pd7iHX0G6xRK6kw/sendMessage?chat_id=-1001135199825&text=' + encodeURIComponent(telegram_msg);
                                            // request.get(url, function (error, response, body) {
                                            //     console.log('send report to channel', response.body)
                                            // })

                                            var url = 'https://slack.com/api/chat.postMessage?token=xoxb-295593484465-Tm4p1TAMo0dOeU588tHaBseS&channel=vasp&text='+encodeURIComponent(telegram_msg)+'&pretty=1';
                                            request.get(url, function (error, response, body) {
                                                console.log('send report to channel', response.body)
                                            })

                                            var skip = config.bulk_skip;
                                            subdb.collection(send.list).count(query, function (err, total) {
                                                if (err) throw err;
                                                console.log('total : ', total);
                                                if (total > 0) {
                                                    console.log(skip);
                                                    function myIter(limit, total) {
                                                        console.log('myIter : ' + limit);
                                                        subdb.collection(send.list).find(query).skip(limit).limit(skip).forEach(function (doc) {
                                                            mobile = doc._id.toString();
                                                            //console.log(mobile);
                                                            if ((mobile.length == 12 && (mobile.startsWith('989'))) || (mobile.length == 10 && (mobile.startsWith('9')))) {
                                                                var mtobj = new Object();
                                                                mtobj.send_id = send.send_id;
                                                                mtobj.username = service.username;
                                                                mtobj.password = service.password;
                                                                mtobj.service_id = service.mt_service_id;
                                                                mtobj.m = send.msg;
                                                                mtobj.o = service.number;
                                                                mtobj.d = mobile;
                                                                mtobj.type = parseInt(send.priority);
                                                                //console.log(mtobj);
                                                                var msg = JSON.stringify(mtobj);
                                                                ch.publish('', mtq, new Buffer(msg), {priority: parseInt(send.priority)});
                                                            }
                                                        }, function (err) {
                                                            // done or error
                                                        });

                                                        setTimeout(function () {
                                                            if (limit + skip <= total) {
                                                                myIter(limit + skip, total);
                                                            } else {
                                                                // end of send
/*                                                                var mtobj = new Object();
                                                                if (send.send_id)
                                                                    mtobj.send_id = send.send_id;
                                                                mtobj.service_key = service.service_key;
                                                                mtobj.msg = 'end_of_send';
                                                                mtobj.from = send.number;
                                                                mtobj.to = '9123032043';
                                                                mtobj.type = parseInt(send.priority);

                                                                mtobj.title = service.title;
                                                                mtobj.op = send.op;
                                                                mtobj.sid = send.sid;
                                                                mtobj.send_id = send.send_id;
                                                                mtobj.total = total;
                                                                mtobj.number = send.number;
                                                                mtobj.list = send.list;
                                                                mtobj.charging = send.charging;
                                                                mtobj.priority = send.priority;
                                                                mtobj.active_users = send.active_users;
                                                                mtobj.omsg = send.msg;

                                                                console.log(mtobj);
                                                                var msg = JSON.stringify(mtobj);
                                                                ch.publish('', mtq, new Buffer(msg), {priority: parseInt(send.priority)});*/
                                                            }
                                                        }, 5000)
                                                    }

                                                    myIter(0, total);
                                                }
                                            });

                                            db.collection("sends").update(
                                                {_id: send._id},
                                                {
                                                    $set: {s: '1', lt: new Date()}
                                                }
                                            );
                                        } else {
                                            telegram_msg = 'ارسال با خطا مواجه شد' + '\n' +
                                                'سرویس تعریف نشده است' + '\n' +
                                                'اپراتور : ' + send.op + '\n' +
                                                'سرویس : ' + send.sid + '\n' +
                                                'ارسال : ' + send.send_id + '\n' +
                                                'تعداد : ' + total + '\n' +
                                                'شماره : ' + send.number + '\n' +
                                                'لیست : ' + send.list + '\n' +
                                                'شارژ : ' + send.charging + '\n' +
                                                'اولویت : ' + send.priority + '\n' +
                                                'کاربران فعال : ' + send.active_users + '\n' +
                                                'پیام : ' + send.msg
                                                +'\n'+'****************';
                                            // var url = 'https://api.telegram.org/bot428825362:AAEgSDWWqKSvk3ao1Ag5pd7iHX0G6xRK6kw/sendMessage?chat_id=-1001135199825&text=' + encodeURIComponent(telegram_msg);
                                            // request.get(url, function (error, response, body) {
                                            //     console.log('send report to channel', response.body)
                                            // })

                                            var url = 'https://slack.com/api/chat.postMessage?token=xoxb-295593484465-Tm4p1TAMo0dOeU588tHaBseS&channel=vasp&text='+encodeURIComponent(telegram_msg)+'&pretty=1';
                                            request.get(url, function (error, response, body) {
                                                console.log('send report to channel', response.body)
                                            })
                                        }
                                    });

                                });
                            }
                        });
                        console.log(new Date());
                    });
                });
            });
        });
    });
});
