MongoClient = require("mongodb").MongoClient;
var cron = require('node-cron');
var http = require('http');
var request = require('request')

MongoClient.connect('mongodb://localhost:27017', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, async function (err, dbClient) {
    if (err) throw err;
    let db = dbClient.db('mohsen');

    cron.schedule('*/5 * * * * *', function () {
        var JSONformData = {
            "organization": "sadraa",
            "username": "sadraa",
            "password": "sadraa4030",
            "method": "getInbound",
            "number": "9820004030",
            "id": 0
        }
        request.post({
            url: 'https://new.payamsms.com/services/rest/index.php',
            headers: {
                'Content-Type': 'application/json'
            },
            json: JSONformData
        }, async function (error, response, body) {
            /*let response = {};
            response.body = {};
            response.body.data = [
                { ID: '679939',
                From: '989011832315',
                To: '9820004030',
                Date: '2021-02-03 12:52:06',
                Body: '0011503271234567' }
            ];*/
            if (response != undefined) {
                if (response.body != undefined) {
                    if (response.body.data != undefined) {
                        for (let entry of response.body.data) {
                            if (entry.ID != undefined && entry.From != undefined && entry.To != undefined && entry.Body != undefined) {
                                entry.Body = entry.Body.replace(/۰/g, '0');
                                entry.Body = entry.Body.replace(/٠/g, '0');
                                entry.Body = entry.Body.replace(/۱/g, '1');
                                entry.Body = entry.Body.replace(/١/g, '1');
                                entry.Body = entry.Body.replace(/۲/g, '2');
                                entry.Body = entry.Body.replace(/٢/g, '2');
                                entry.Body = entry.Body.replace(/۳/g, '3');
                                entry.Body = entry.Body.replace(/٣/g, '3');
                                entry.Body = entry.Body.replace(/۴/g, '4');
                                entry.Body = entry.Body.replace(/٤/g, '4');
                                entry.Body = entry.Body.replace(/۵/g, '5');
                                entry.Body = entry.Body.replace(/٥/g, '5');
                                entry.Body = entry.Body.replace(/۶/g, '6');
                                entry.Body = entry.Body.replace(/٦/g, '6');
                                entry.Body = entry.Body.replace(/۷/g, '7');
                                entry.Body = entry.Body.replace(/٧/g, '7');
                                entry.Body = entry.Body.replace(/۸/g, '8');
                                entry.Body = entry.Body.replace(/۹/g, '9');
                                entry.Body = entry.Body.replace(/٩/g, '9');
                                let numb = entry.Body.match(/\d/g);
                                let pinCode = '';
                                let rightFlag = false;
                                if (numb)
                                    pinCode = numb.join("");
                                entry.pin = pinCode;
                                let branchCode = '';
                                let bCode = '';
                                let branchId = 0;
                                let branchName = '';
                                if (pinCode.length == 15 || pinCode.length == 16) {
                                    branchCode = 'b' + pinCode[0] + pinCode[1] + pinCode[2] + pinCode[3] + pinCode[4];
                                    bCode = pinCode[0] + pinCode[1] + pinCode[2] + pinCode[3] + pinCode[4];
                                    branchId = parseInt(bCode);
                                }
                                bCode = bCode.toString();
                                entry.branchId = branchId;
                                await db.collection('plans').findOneAndUpdate(
                                    {s: 1},
                                    {$inc: {'count': 1}}, async function (err, incPlan) {
                                        let customerId = incPlan.value.count + 1;
                                        entry.customerId = customerId;
                                        if (pinCode.length == 15 || pinCode.length == 16) {
                                            let r = await db.collection('msg').findOne({pin: pinCode, status:200});
                                            if (r) {
                                                if (r.From == entry.From) {
                                                    // send sms duplicate you!
                                                    entry.plan = incPlan.value.planId;
                                                    entry.status = 301;
                                                    entry.code = 0;
                                                    entry.time = new Date();
                                                    entry.msg = 'فروشگاه محسن\n' +
                                                        'جایزه ویژه هر هفته یک دستگاه PS5\n' +
                                                        'مشتری گرامی کد ارسالی قبلا توسط خودتان ثبت شده است\n' +
                                                        'پخش زنده قرعه کشی در لایو اینستاگرام فروشگاه محسن هر دوشنبه ساعت ۱۴\n' +
                                                        'http://instagram.com/mohsen.stores\n' +
                                                        'www.mohsen.ir\n' +
                                                        'در صورت بروز هرگونه مشکل با شماره زیر تماس بگیرید\n' +
                                                        '۰۲۱-۸۸۵۵۹۱۹۱';
                                                    var JSONformData = {
                                                        "organization": "sadraa",
                                                        "username": "sadraa",
                                                        "password": "sadraa4030",
                                                        "method": "send",
                                                        "messages": [{
                                                            "sender": "9820004030",
                                                            "recipient": entry.From,
                                                            "body": entry.msg,
                                                            "customerId": customerId
                                                        }]
                                                    }
                                                    request.post({
                                                        url: 'https://new.payamsms.com/services/rest/index.php',
                                                        headers: {
                                                            'Content-Type': 'application/json'
                                                        },
                                                        json: JSONformData
                                                    }, function (error, response, body) {
                                                        entry.res = response.body;
                                                        db.collection('msg').insertOne(entry, async function (errSMS, insSMS) {
                                                            console.log(entry)
                                                        });
                                                    })
                                                } else {
                                                // send sms duplicate other!
                                                    entry.plan = incPlan.value.planId;
                                                    entry.status = 302;
                                                    entry.code = 0;
                                                    entry.time = new Date();
                                                    entry.msg = 'فروشگاه محسن\n' +
                                                        'جایزه ویژه هر هفته یک دستگاه PS5\n' +
                                                        'مشتری گرامی کد ارسالی قبلا توسط شماره دیگری ثبت شده است\n' +
                                                        'پخش زنده قرعه کشی در لایو اینستاگرام فروشگاه محسن هر دوشنبه ساعت ۱۴\n' +
                                                        'http://instagram.com/mohsen.stores\n' +
                                                        'www.mohsen.ir\n' +
                                                        'در صورت بروز هرگونه مشکل با شماره زیر تماس بگیرید\n' +
                                                        '۰۲۱-۸۸۵۵۹۱۹۱';
                                                    var JSONformData = {
                                                        "organization": "sadraa",
                                                        "username": "sadraa",
                                                        "password": "sadraa4030",
                                                        "method": "send",
                                                        "messages": [{
                                                            "sender": "9820004030",
                                                            "recipient": entry.From,
                                                            "body": entry.msg,
                                                            "customerId": customerId
                                                        }]
                                                    }
                                                    request.post({
                                                        url: 'https://new.payamsms.com/services/rest/index.php',
                                                        headers: {
                                                            'Content-Type': 'application/json'
                                                        },
                                                        json: JSONformData
                                                    }, function (error, response, body) {
                                                        entry.res = response.body;
                                                        db.collection('msg').insertOne(entry, async function (errSMS, insSMS) {
                                                            console.log(entry)
                                                        });
                                                    })
                                                }
                                            } else {
                                            // else duplicate full code
                                                let isValid = false;
                                                if (pinCode.length == 15 || pinCode.length == 16) {
                                                    if (branchId != 0 && (pinCode[5] == 0 || pinCode[5] == 1) && (pinCode[6] == 0 || pinCode[6] == 1 || pinCode[6] == 2 || pinCode[6] == 3 || pinCode[6] == 4 || pinCode[6] == 5 || pinCode[6] == 6 || pinCode[6] == 7 || pinCode[6] == 8 || pinCode[6] == 9) &&
                                                        (pinCode[7] == 0 || pinCode[7] == 1 || pinCode[7] == 2 || pinCode[7] == 3)) {
                                                        isValid = true;
                                                    }
                                                }
                                                if (isValid) {
                                                    let keyword = '';
                                                    if (pinCode.length == 15) {
                                                        keyword = pinCode[9] + pinCode[10] + pinCode[11] + pinCode[12] + pinCode[13] + pinCode[14];
                                                    }
                                                    if (pinCode.length == 16) {
                                                        keyword = pinCode[9] + pinCode[10] + pinCode[11] + pinCode[12] + pinCode[13] + pinCode[14] + pinCode[15];
                                                    }
                                                    let chkOrderNumber = await db.collection('msg').find({status:200, plan: incPlan.value.planId, pin: {$regex: keyword + "$"}, $where: "this.pin.length > 15" }).toArray();
                                                    if (chkOrderNumber.length) {
                                                        if (chkOrderNumber[0].From == entry.From) {
                                                            // send sms duplicate you!
                                                            entry.plan = incPlan.value.planId;
                                                            entry.status = 401;
                                                            entry.code = 0;
                                                            entry.time = new Date();
                                                            entry.msg = 'فروشگاه محسن\n' +
                                                                'جایزه ویژه هر هفته یک دستگاه PS5\n' +
                                                                'مشتری گرامی کد ارسالی قبلا توسط خودتان ثبت شده است\n' +
                                                                'پخش زنده قرعه کشی در لایو اینستاگرام فروشگاه محسن هر دوشنبه ساعت ۱۴\n' +
                                                                'http://instagram.com/mohsen.stores\n' +
                                                                'www.mohsen.ir\n' +
                                                                'در صورت بروز هرگونه مشکل با شماره زیر تماس بگیرید\n' +
                                                                '۰۲۱-۸۸۵۵۹۱۹۱';
                                                            var JSONformData = {
                                                                "organization": "sadraa",
                                                                "username": "sadraa",
                                                                "password": "sadraa4030",
                                                                "method": "send",
                                                                "messages": [{
                                                                    "sender": "9820004030",
                                                                    "recipient": entry.From,
                                                                    "body": entry.msg,
                                                                    "customerId": customerId
                                                                }]
                                                            }
                                                            request.post({
                                                                url: 'https://new.payamsms.com/services/rest/index.php',
                                                                headers: {
                                                                    'Content-Type': 'application/json'
                                                                },
                                                                json: JSONformData
                                                            }, function (error, response, body) {
                                                                entry.res = response.body;
                                                                db.collection('msg').insertOne(entry, async function (errSMS, insSMS) {
                                                                    console.log(entry)
                                                                });
                                                            })
                                                        } else {
                                                        // send sms duplicate other!
                                                            entry.plan = incPlan.value.planId;
                                                            entry.status = 402;
                                                            entry.code = 0;
                                                            entry.time = new Date();
                                                            entry.msg = 'فروشگاه محسن\n' +
                                                                'جایزه ویژه هر هفته یک دستگاه PS5\n' +
                                                                'مشتری گرامی کد ارسالی قبلا توسط شماره دیگری ثبت شده است\n' +
                                                                'پخش زنده قرعه کشی در لایو اینستاگرام فروشگاه محسن هر دوشنبه ساعت ۱۴\n' +
                                                                'http://instagram.com/mohsen.stores\n' +
                                                                'www.mohsen.ir\n' +
                                                                'در صورت بروز هرگونه مشکل با شماره زیر تماس بگیرید\n' +
                                                                '۰۲۱-۸۸۵۵۹۱۹۱';
                                                            var JSONformData = {
                                                                "organization": "sadraa",
                                                                "username": "sadraa",
                                                                "password": "sadraa4030",
                                                                "method": "send",
                                                                "messages": [{
                                                                    "sender": "9820004030",
                                                                    "recipient": entry.From,
                                                                    "body": entry.msg,
                                                                    "customerId": customerId
                                                                }]
                                                            }
                                                            request.post({
                                                                url: 'https://new.payamsms.com/services/rest/index.php',
                                                                headers: {
                                                                    'Content-Type': 'application/json'
                                                                },
                                                                json: JSONformData
                                                            }, function (error, response, body) {
                                                                entry.res = response.body;
                                                                db.collection('msg').insertOne(entry, async function (errSMS, insSMS) {
                                                                    console.log(entry)
                                                                });
                                                            })
                                                        }
                                                    } else {
                                                    // else duplicate factor code
                                                        await db.collection('plans').findOneAndUpdate(
                                                            {s: 1},
                                                            {$inc: {'row': 1}}, async function (err, incRow) {
                                                                let newCode = incRow.value.row + 1;
                                                                if (isNaN(newCode))
                                                                    newCode = 1;
                                                                let msg = incRow.value.resMsg;
                                                                let txtCode = '';
                                                                if (newCode < 100000)
                                                                    txtCode = '0' + newCode;
                                                                if (newCode < 10000)
                                                                    txtCode = '00' + newCode;
                                                                if (newCode < 1000)
                                                                    txtCode = '000' + newCode;
                                                                if (newCode < 100)
                                                                    txtCode = '0000' + newCode;
                                                                if (newCode < 10)
                                                                    txtCode = '00000' + newCode;
                                                                msg = msg.replace('[ROW]', txtCode);
                                                                entry.plan = incRow.value.planId;
                                                                entry.status = 200;
                                                                entry.code = newCode;
                                                                entry.msg = msg;
                                                                entry.time = new Date();
                                                                var JSONformData = {
                                                                    "organization": "sadraa",
                                                                    "username": "sadraa",
                                                                    "password": "sadraa4030",
                                                                    "method": "send",
                                                                    "messages": [{
                                                                        "sender": "9820004030",
                                                                        "recipient": entry.From,
                                                                        "body": entry.msg,
                                                                        "customerId": customerId
                                                                    }]
                                                                }
                                                                request.post({
                                                                    url: 'https://new.payamsms.com/services/rest/index.php',
                                                                    headers: {
                                                                        'Content-Type': 'application/json'
                                                                    },
                                                                    json: JSONformData
                                                                }, function (error, response, body) {
                                                                    entry.res = response.body;
                                                                    db.collection('msg').insertOne(entry, async function (errSMS, insSMS) {
                                                                        console.log(entry)
                                                                    });
                                                                })
                                                            });
                                                    // end duplicate factor code
                                                    }
                                                } else {
                                                // else not valid code
                                                    entry.plan = incPlan.value.planId;
                                                    entry.status = 201;
                                                    entry.code = 0;
                                                    entry.time = new Date();
                                                    entry.msg = 'فروشگاه محسن\n' +
                                                        'جایزه ویژه هر هفته یک دستگاه PS5\n' +
                                                        'مشتری گرامی کد ارسالی نامعتبر میباشد، لطفا کد صحیح را ارسال فرمایید\n' +
                                                        'پخش زنده قرعه کشی در لایو اینستاگرام فروشگاه محسن هر دوشنبه ساعت ۱۴\n' +
                                                        'http://instagram.com/mohsen.stores\n' +
                                                        'www.mohsen.ir\n' +
                                                        'در صورت بروز هرگونه مشکل با شماره زیر تماس بگیرید\n' +
                                                        '۰۲۱-۸۸۵۵۹۱۹۱';
                                                    var JSONformData = {
                                                        "organization": "sadraa",
                                                        "username": "sadraa",
                                                        "password": "sadraa4030",
                                                        "method": "send",
                                                        "messages": [{
                                                            "sender": "9820004030",
                                                            "recipient": entry.From,
                                                            "body": entry.msg,
                                                            "customerId": customerId
                                                        }]
                                                    }
                                                    request.post({
                                                        url: 'https://new.payamsms.com/services/rest/index.php',
                                                        headers: {
                                                            'Content-Type': 'application/json'
                                                        },
                                                        json: JSONformData
                                                    }, function (error, response, body) {
                                                        entry.res = response.body;
                                                        db.collection('msg').insertOne(entry, async function (errSMS, insSMS) {
                                                            console.log(entry)
                                                        });
                                                    })
                                                // end not valid code
                                                }
                                            // end duplicate code
                                            }
                                        } else {
                                        // else (pinCode.length == 15 || pinCode.length == 16)
                                            entry.plan = incPlan.value.planId;
                                            entry.status = 202;
                                            entry.code = 0;
                                            entry.time = new Date();
                                            entry.msg = 'فروشگاه محسن\n' +
                                                'جایزه ویژه هر هفته یک دستگاه PS5\n' +
                                                'مشتری گرامی کد ارسالی نامعتبر میباشد، لطفا کد صحیح را ارسال فرمایید\n' +
                                                'پخش زنده قرعه کشی در لایو اینستاگرام فروشگاه محسن هر دوشنبه ساعت ۱۴\n' +
                                                'http://instagram.com/mohsen.stores\n' +
                                                'www.mohsen.ir\n' +
                                                'در صورت بروز هرگونه مشکل با شماره زیر تماس بگیرید\n' +
                                                '۰۲۱-۸۸۵۵۹۱۹۱';
                                            var JSONformData = {
                                                "organization": "sadraa",
                                                "username": "sadraa",
                                                "password": "sadraa4030",
                                                "method": "send",
                                                "messages": [{
                                                    "sender": "9820004030",
                                                    "recipient": entry.From,
                                                    "body": entry.msg,
                                                    "customerId": customerId
                                                }]
                                            }
                                            request.post({
                                                url: 'https://new.payamsms.com/services/rest/index.php',
                                                headers: {
                                                    'Content-Type': 'application/json'
                                                },
                                                json: JSONformData
                                            }, function (error, response, body) {
                                                entry.res = response.body;
                                                db.collection('msg').insertOne(entry, async function (errSMS, insSMS) {
                                                    console.log(entry)
                                                });
                                            })
                                        // end (pinCode.length == 15 || pinCode.length == 16)
                                        }
                                    });
                            }
                        }
                    } else {
                        console.log('response.body.data is null');
                    }
                } else {
                    console.log('response.body is null');
                }
            } else {
                console.log('response is null');
            }
        })//end of request get sms
    });// end of cron
});