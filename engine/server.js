var express = require('express');
var app = express();
var MongoClient = require("mongodb").MongoClient;

var service = {
    WebService : {
        WebServiceSoap :{
            activate : function(args) {
                console.log('activate');
                console.log(args);
                var ret = 2;
                var date = new Date();
                var ty = date.getFullYear();
                var tm = date.getMonth() + 1;
                var td = date.getDate();
                MongoClient.connect(config.dbs.mongo_subs_history, function (err, dbhistory) {
                    if (err) throw err;
                    console.log('activate1');
                    MongoClient.connect(config.dbs.mongo_subs, function (err, dbsh) {
                        if (err) throw err;
                        console.log('activate2');
                        MongoClient.connect(config.dbs.mongo_mo, function (err, db) {
                            if (err) throw err;
                            console.log('activate3');
                            dbsh.collection("rightel_sub_" + args.Serviceid).count({
                                _id: args.MSISDN,
                                s: 1
                            }, function (err, total) {
                                if (err) throw err;
                                if (total == 0) {
                                    // subscribe user
                                    dbsh.collection("rightel_sub_" + args.Serviceid).update(
                                        {_id: args.MSISDN},
                                        {
                                            $set: {s: 1, lt: new Date()},
                                            $setOnInsert: {t: new Date()}
                                        },
                                        {upsert: true}
                                    );
                                    // sub/unsub history
                                    dbhistory.collection("rightel_sub_history_" + tm).insertOne({
                                        m: args.MSISDN,
                                        sid: args.Serviceid,
                                        sub: 1,
                                        t: new Date()
                                    }, function (err, res) {
                                        if (err) console.log(err);
                                    });
                                    ret = 0;
                                    return ret;
                                } else {
                                    //Already activated
                                    ret = 1;
                                    return ret;
                                }
                            });
                            return ret;
                        });
                    });
                });
            },
            inactivate : function(args) {
                console.log('inactivate');
                console.log(args);
                var ret = 2;
                var date = new Date();
                var ty = date.getFullYear();
                var tm = date.getMonth() + 1;
                var td = date.getDate();
                MongoClient.connect(config.dbs.mongo_subs_history, function (err, dbhistory) {
                    if (err) throw err;
                    console.log('inactivate1');
                    MongoClient.connect(config.dbs.mongo_subs, function (err, dbsh) {
                        if (err) throw err;
                        console.log('inactivate2');
                        MongoClient.connect(config.dbs.mongo_mo, function (err, db) {
                            if (err) throw err;
                            console.log('inactivate3');
                            dbsh.collection("rightel_sub_" + args.Serviceid).count({
                                _id: args.MSISDN,
                                s: 0
                            }, function (err, total) {
                                if (err) throw err;
                                if (total == 0) {
                                    // subscribe user
                                    dbsh.collection("rightel_sub_" + args.Serviceid).update(
                                        {_id: args.MSISDN},
                                        {
                                            $set: {s: 0, lt: new Date()},
                                            $setOnInsert: {t: new Date()}
                                        },
                                        {upsert: true}
                                    );
                                    // sub/unsub history
                                    dbhistory.collection("rightel_sub_history_" + tm).insertOne({
                                        m: args.MSISDN,
                                        sid: args.Serviceid,
                                        sub: 0,
                                        t: new Date()
                                    }, function (err, res) {
                                        if (err) console.log(err);
                                    });
                                    ret = 0;
                                    return ret;
                                } else {
                                    //Already inactivated
                                    ret = 1;
                                    return ret;
                                }
                            });
                            return ret;
                        });
                    });
                });
            },
            notify : function(args) {
                console.log('notify');
                console.log(args);
                var ret = 3;
                var date = new Date();
                var ty = date.getFullYear();
                var tm = date.getMonth() + 1;
                var td = date.getDate();
                MongoClient.connect(config.dbs.mongo_subs_history, function (err, dbhistory) {
                    if (err) throw err;
                    MongoClient.connect(config.dbs.mongo_subs, function (err, dbsh) {
                        if (err) throw err;
                        MongoClient.connect(config.dbs.mongo_mo, function (err, db) {
                            if (err) throw err;
                            db.collection("rightel_mo_" + ty + tm + td).insertOne({
                                o: args.MSISDN,
                                s: args.Serviceid,
                                m: args.Message,
                                t: date
                            }, function (err, res) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    dbsh.collection("rightel_sub_" + args.Serviceid).count({
                                        _id: args.MSISDN,
                                        s: 1
                                    }, function (err, total) {
                                        if (err) throw err;
                                        if (total == 0) {
                                            // subscribe user
                                            dbsh.collection("rightel_sub_" + args.Serviceid).update(
                                                {_id: args.MSISDN},
                                                {
                                                    $set: {s: 0, lt: new Date()},
                                                    $setOnInsert: {t: new Date()}
                                                },
                                                {upsert: true}
                                            );
                                            // sub/unsub history
                                            dbhistory.collection("rightel_sub_history_" + tm).insertOne({
                                                m: args.MSISDN,
                                                sid: args.Serviceid,
                                                sub: 0,
                                                t: new Date()
                                            }, function (err, res) {
                                                if (err) console.log(err);
                                            });
                                            ret = 0;
                                            return ret;
                                        } else {
                                            //Already inactivated
                                            ret = 1;
                                            return ret;
                                        }
                                    });
                                }
                            });
                            return ret;
                        });
                    });
                });
            }
        }
    }
}

// xml data is extracted from wsdl file created
var xml = require('fs').readFileSync('/var/www/html/vas/engine/rightel/ws.wsdl','utf8');
//create an express server and pass it to a soap server
var server = app.listen(4001,function(){
    var host = "172.19.2.196";
    //var host = "79.175.139.180";
    var port = server.address().port;
});
soap.listen(server,'/rightel',service,xml);