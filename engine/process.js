amqp = require("amqplib/callback_api");
MongoClient = require("mongodb").MongoClient;

MongoClient.connect('mongodb://localhost:27017', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, function (err, dbClient) {
    if (err) throw err;
    let db = dbClient.db('sadraa');

    amqp.connect("amqp://localhost", function (err, conn) {
        conn.createChannel(function (err, ch) {
            let q = "mo",
                mtq = "mt";
            ch.assertQueue(q, {durable: true, maxPriority: 10});
            ch.assertQueue(mtq, {durable: true, maxPriority: 10});

            ch.prefetch(1);
            ch.consume(q, function (msg) {
                    var secs = msg.content.toString().split(".").length - 1;
                    var str = msg.content.toString("utf8");
                    var myObj = JSON.parse(str);
                    myObj.text = myObj.text.replace(/۰/g, '0');
                    myObj.text = myObj.text.replace(/٠/g, '0');
                    myObj.text = myObj.text.replace(/۱/g, '1');
                    myObj.text = myObj.text.replace(/١/g, '1');
                    myObj.text = myObj.text.replace(/۲/g, '2');
                    myObj.text = myObj.text.replace(/٢/g, '2');
                    myObj.text = myObj.text.replace(/۳/g, '3');
                    myObj.text = myObj.text.replace(/٣/g, '3');
                    myObj.text = myObj.text.replace(/۴/g, '4');
                    myObj.text = myObj.text.replace(/٤/g, '4');
                    myObj.text = myObj.text.replace(/۵/g, '5');
                    myObj.text = myObj.text.replace(/٥/g, '5');
                    myObj.text = myObj.text.replace(/۶/g, '6');
                    myObj.text = myObj.text.replace(/٦/g, '6');
                    myObj.text = myObj.text.replace(/۷/g, '7');
                    myObj.text = myObj.text.replace(/٧/g, '7');
                    myObj.text = myObj.text.replace(/۸/g, '8');
                    myObj.text = myObj.text.replace(/۹/g, '9');
                    myObj.text = myObj.text.replace(/٩/g, '9');
                    let numb = myObj.text.match(/\d/g);
                    let pinCode = '';
                    if (numb)
                        pinCode = numb.join("");
                    let existFlag = false;
                    let coll = '';
                    let mobile = myObj.from;
                    let code;
                    let cat = 0;

                    if (pinCode == '') {
                        db.collection('comments').insertOne({
                            s: 1,
                            f: myObj.to,
                            m: myObj.from,
                            b: myObj.text,
                            t: new Date()
                        }, async function (errSMS, insSMS) {
                            console.log('comment inserted')
                        });
                    } else {
                        //get current plan
                        db.collection('plans').findOne({s: 1}, async function (err, resPlan) {
                            if (err) console.log(err);
                            for (let i = 0; i < resPlan.collections.length; i++) {
                                let r = await db.collection(resPlan.collections[i]).findOne({p: pinCode});
                                if (r) {
                                    existFlag = true;
                                    coll = resPlan.collections[i];
                                    code = r;
                                    cat = parseInt(coll.replace('cat-', ''), 10);
                                }
                            }
                            if (existFlag == false) {
                                var mtObj = new Object();
                                mtObj.from = myObj.to;
                                mtObj.to = myObj.from;
                                mtObj.status = 201;
                                mtObj.cat = cat;
                                mtObj.coll = coll;
                                mtObj.planId = resPlan.planId;
                                mtObj.row = 0;
                                mtObj.pinCode = pinCode;
                                mtObj.text = myObj.text;
                                mtObj.msg = resPlan.wrongMsg;
                                let mtMsg = JSON.stringify(mtObj);
                                ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                // send sms pinCode not found !
                            } else {
                                if (code.m != undefined) {
                                    if (code.m == mobile) {
                                        var mtObj = new Object();
                                        mtObj.from = myObj.to;
                                        mtObj.to = myObj.from;
                                        mtObj.status = 204;
                                        mtObj.cat = cat;
                                        mtObj.coll = coll;
                                        mtObj.planId = resPlan.planId;
                                        mtObj.row = 0;
                                        mtObj.pinCode = pinCode;
                                        mtObj.text = myObj.text;
                                        mtObj.msg = resPlan.dupYouMsg;
                                        let mtMsg = JSON.stringify(mtObj);
                                        ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                        // send sms duplicate you 1 !
                                    } else {
                                        var mtObj = new Object();
                                        mtObj.from = myObj.to;
                                        mtObj.to = myObj.from;
                                        mtObj.status = 202;
                                        mtObj.cat = cat;
                                        mtObj.coll = coll;
                                        mtObj.planId = resPlan.planId;
                                        mtObj.row = 0;
                                        mtObj.pinCode = pinCode;
                                        mtObj.text = myObj.text;
                                        mtObj.msg = resPlan.dupOthersMsg;
                                        let mtMsg = JSON.stringify(mtObj);
                                        ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                        // send sms duplicate other 1 !
                                    }
                                } else {
                                    db.collection('old_sms').findOne({$or: [{bf: pinCode}, {bf: 'M,' + pinCode}]}, async function (errSMS, resSMS) {
                                        if (errSMS) console.log(errSMS);
                                        if (resSMS) {
                                            if (resSMS.m != undefined) {
                                                if (resSMS.m == mobile) {
                                                    var mtObj = new Object();
                                                    mtObj.from = myObj.to;
                                                    mtObj.to = myObj.from;
                                                    mtObj.status = 204;
                                                    mtObj.cat = cat;
                                                    mtObj.coll = coll;
                                                    mtObj.planId = resPlan.planId;
                                                    mtObj.row = 0;
                                                    mtObj.pinCode = pinCode;
                                                    mtObj.text = myObj.text;
                                                    mtObj.msg = resPlan.dupYouMsg;
                                                    let mtMsg = JSON.stringify(mtObj);
                                                    ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                                    // send sms duplicate you in old 1 !
                                                } else {
                                                    var mtObj = new Object();
                                                    mtObj.from = myObj.to;
                                                    mtObj.to = myObj.from;
                                                    mtObj.status = 202;
                                                    mtObj.cat = cat;
                                                    mtObj.coll = coll;
                                                    mtObj.planId = resPlan.planId;
                                                    mtObj.row = 0;
                                                    mtObj.pinCode = pinCode;
                                                    mtObj.text = myObj.text;
                                                    mtObj.msg = resPlan.dupOthersMsg;
                                                    let mtMsg = JSON.stringify(mtObj);
                                                    ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                                    // send sms duplicate other in old 1 !
                                                }
                                            }
                                        } else {
                                            db.collection(coll).updateOne(
                                                {p: pinCode},
                                                {
                                                    $set: {
                                                        v: 1,
                                                        m: mobile,
                                                        t: new Date(),
                                                        l: resPlan.planId
                                                    }
                                                }, async function (err, updCode) {
                                                    db.collection('plans').findOneAndUpdate(
                                                        {planId: resPlan.planId},
                                                        {$inc: {row: 1}}, async function (err, updPlan) {
                                                            let newCode = updPlan.value.row + 1;
                                                            let msg = updPlan.value.resMsg;
                                                            msg = msg.replace('[ROW]', newCode);
                                                            var mtObj = new Object();
                                                            mtObj.from = myObj.to;
                                                            mtObj.to = myObj.from;
                                                            mtObj.status = 200;
                                                            mtObj.cat = cat;
                                                            mtObj.coll = coll;
                                                            mtObj.planId = updPlan.value.planId;
                                                            mtObj.row = newCode;
                                                            mtObj.pinCode = pinCode;
                                                            mtObj.text = myObj.text;
                                                            mtObj.msg = msg;
                                                            let mtMsg = JSON.stringify(mtObj);
                                                            ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                                            // send sms new row !
                                                        });
                                                });
                                        }
                                    });
                                }
                            }
                        });
                    }
                    setTimeout(function () {
                        ch.ack(msg);
                    }, secs * 1000)
                }, {
                    noAck: false
                }
            );
        });
    });
});