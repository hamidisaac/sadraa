var config = require("../config");
var express        =         require("express");
var bodyParser     =         require("body-parser");
var app            =         express();
var amqp = require('amqplib/callback_api');
var url = require('url');
process.env.TZ = 'Asia/Tehran';

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

process.env.TZ = 'Asia/Tehran';
amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        var q = 'rightel_notify';

        app.get('/rightel_notify',function(req,res){
            var parsedUrl = url.parse(req.url, true); // true to get query as object
            var msg = JSON.stringify(parsedUrl.query);

            ch.assertQueue(q, {durable: true});
            ch.sendToQueue(q, new Buffer(msg), {persistent: true});
            console.log(" [x] 1 Sent '%s'", msg);
            res.end("ok");
        });
        app.post('/rightel_notify',function(req,res){
            var msg = JSON.stringify(req.body);
            ch.assertQueue(q, {durable: true});
            ch.sendToQueue(q, new Buffer(msg), {persistent: true});
            console.log(" [x] 2 Sent '%s'", msg);
            res.end("ok");
        });
    });
});
app.listen(config.rightel_notify_port,function(){
    console.log("Started on PORT "+config.rightel_notify_port);
})