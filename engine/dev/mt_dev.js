var http = require('http');
var amqp = require("amqplib/callback_api");
var MongoClient = require("mongodb").MongoClient;
var soap = require('strong-soap').soap;

MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser: true, useUnifiedTopology: true}, function (err, dbClient) {
    if (err) throw err;
    let db = dbClient.db('sadraa_dev');

    amqp.connect("amqp://localhost", function (err, conn) {
        conn.createChannel(function (err, ch) {
            var q = "mt_dev";
            ch.assertQueue(q, {durable: true, maxPriority: 10});
            ch.prefetch(10);
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
            ch.consume(q, function (msg) {
                var secs = msg.content.toString().split(".").length - 1;
                var str = msg.content.toString("utf8");
                var myObj = JSON.parse(str);

                myObj.t = new Date();
                myObj.r = 0;

                var url = 'http://ws2.adpdigital.com/services/MessagingService?wsdl';
                var args = {
                    userName : 'parnian4030',
                    password : 'parnian4030',
                    destNo : '98'+myObj.To,
                    sourceNo : 98200004030,
                    sourcePort : null,
                    destPort : null,
                    clientId : 1 ,
                    messageType : 1,
                    encoding : 2,
                    longSupported : 0,
                    dueTime : new Date(),
                    content : myObj.MessageBody
                };
                var options = {};
                soap.createClient(url, options, function(err, client) {
                    var method = client['send'];
                    method(args, function(err, result, envelope, soapHeader) {
                        db.collection('sms_'+myObj.planId).insertOne({
                            id: myObj.row,
                            s: myObj.status,
                            b: myObj.text,
                            c: myObj.cat,
                            l: myObj.coll,
                            p: myObj.pinCode,
                            f: myObj.From,
                            m: myObj.To,
                            o: myObj.MessageBody,
                            r: result,
                            t: new Date()
                        }, async function (errSMS, insSMS) {
                        });
                    });
                });
                setTimeout(function () {
                    console.log(" [x] Done");
                    ch.ack(msg);
                }, 5000);
            }, {
                noAck: false
            });
        });
    });
});
