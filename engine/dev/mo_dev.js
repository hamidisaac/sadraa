var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var MongoClient = require("mongodb").MongoClient;
var url = require('url');
var amqp = require('amqplib/callback_api');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies

MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser: true, useUnifiedTopology: true}, function (err, dbClient) {
    if (err) throw err;
    let db = dbClient.db('sadraa_dev');
    amqp.connect('amqp://localhost', function (err, conn) {
        conn.createChannel(function (err, ch) {
            let q = 'mo_dev';
            ch.assertQueue(q, {durable: true, maxPriority: 10});
            app.get('/getsms', function (req, res) {
                var trustedIps = [
                    '127.0.0.1'
                ];
                var ip = req.connection.remoteAddress;
                ip = ip.replace('::ffff:', '');
                db.collection('mo').insertOne({
                    ip: ip,
                    u: req.url,
                    b: req.body,
                    t: new Date()
                }, async function (errSMS, insSMS) {
                    console.log('mo inserted')
                });
                if (trustedIps.indexOf(ip) >= 0 || true) {
                    if (req.query.From.startsWith('98') && (req.query.From.length == 12 || req.query.From.length == 13))
                        req.query.From = req.query.From.replace("98", "");
                    if (req.query.From.startsWith('0') && req.query.From.length == 11)
                        req.query.From = req.query.From.replace("0", "");
                    let msg = JSON.stringify(req.query);
                    ch.sendToQueue(q, new Buffer(msg), {persistent: true});
                    console.log("Sent to mo queue : '%s'", msg);
                    res.end("ok");
                } else {
                    res.end("nok" + ip);
                }
            });
        });
    });
});

app.listen(4000, function () {
    console.log("Started on PORT " + 4000);
})