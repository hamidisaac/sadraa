amqp = require("amqplib/callback_api"),
MongoClient = require("mongodb").MongoClient,

MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser: true, useUnifiedTopology: true}, function (err, dbClient) {
    if (err) throw err;
    let db = dbClient.db('sadraa_dev');

    amqp.connect("amqp://localhost", function (err, conn) {
        conn.createChannel(function (err, ch) {
            let q = "mo_dev",
                mtq = "mt_dev";
            let ackFlag = false;
            ch.assertQueue(q, {durable: true, maxPriority: 10});
            ch.assertQueue(mtq, {durable: true, maxPriority: 10});

            ch.prefetch(5);
            ch.consume(q, function (msg) {
                ackFlag = false;
                    var secs = msg.content.toString().split(".").length - 1;
                    var str = msg.content.toString("utf8");
                    var myObj = JSON.parse(str);
                    let numb = myObj.MessageBody.match(/\d/g);
                    let pinCode = '';
                    if(numb)
                        pinCode = numb.join("");
                    let existFlag = false;
                    let coll = '';
                    let mobile = myObj.From;
                    let code;
                    let cat = 0;

                    if(pinCode == '') {
                        db.collection('comments').insertOne({
                            s: 1,
                            f: myObj.To,
                            m: myObj.From,
                            b: myObj.MessageBody,
                            t: new Date()
                        }, async function (errSMS, insSMS) {
                            console.log('comment inserted')
                        });
                    } else {
                        //get current plan
                        db.collection('plans').findOne({s: 1}, async function (err, resPlan) {
                            if (err) console.log(err);
                            if(resPlan) {
                                ackFlag = true;
                                for (let i = 0; i < resPlan.collections.length; i++) {
                                    let r = await db.collection(resPlan.collections[i]).findOne({p: pinCode});
                                    if (r) {
                                        existFlag = true;
                                        coll = resPlan.collections[i];
                                        code = r;
                                        cat = parseInt(coll.replace('cat-', ''), 10);
                                    }
                                }
                                if (existFlag == false) {
                                    var mtObj = new Object();
                                    mtObj.From = myObj.To;
                                    mtObj.To = myObj.From;
                                    mtObj.status = 201;
                                    mtObj.cat = cat;
                                    mtObj.coll = coll;
                                    mtObj.planId = resPlan.planId;
                                    mtObj.row = 0;
                                    mtObj.pinCode = pinCode;
                                    mtObj.text = myObj.MessageBody;
                                    mtObj.MessageBody = resPlan.wrongMsg;
                                    let mtMsg = JSON.stringify(mtObj);
                                    ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                    // send sms pinCode not found !
                                } else {
                                    if (code.m != undefined) {
                                        if (code.m == mobile) {
                                            var mtObj = new Object();
                                            mtObj.From = myObj.To;
                                            mtObj.To = myObj.From;
                                            mtObj.status = 204;
                                            mtObj.cat = cat;
                                            mtObj.coll = coll;
                                            mtObj.planId = resPlan.planId;
                                            mtObj.row = 0;
                                            mtObj.pinCode = pinCode;
                                            mtObj.text = myObj.MessageBody;
                                            mtObj.MessageBody = msg;
                                            mtObj.MessageBody = resPlan.dupYouMsg;
                                            let mtMsg = JSON.stringify(mtObj);
                                            ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                            // send sms duplicate you 1 !
                                        } else {
                                            var mtObj = new Object();
                                            mtObj.From = myObj.To;
                                            mtObj.To = myObj.From;
                                            mtObj.status = 202;
                                            mtObj.cat = cat;
                                            mtObj.coll = coll;
                                            mtObj.planId = resPlan.planId;
                                            mtObj.row = 0;
                                            mtObj.pinCode = pinCode;
                                            mtObj.text = myObj.MessageBody;
                                            mtObj.MessageBody = msg;
                                            mtObj.MessageBody = resPlan.dupOthersMsg;
                                            let mtMsg = JSON.stringify(mtObj);
                                            ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                            // send sms duplicate other 1 !
                                        }
                                    } else {
                                        db.collection('old_sms').findOne({p: pinCode}, async function (errSMS, resSMS) {
                                            if (errSMS) console.log(errSMS);
                                            if (resSMS) {
                                                if (resSMS.m != undefined) {
                                                    if (resSMS.m == mobile) {
                                                        var mtObj = new Object();
                                                        mtObj.From = myObj.To;
                                                        mtObj.To = myObj.From;
                                                        mtObj.status = 204;
                                                        mtObj.cat = cat;
                                                        mtObj.coll = coll;
                                                        mtObj.planId = resPlan.planId;
                                                        mtObj.row = 0;
                                                        mtObj.pinCode = pinCode;
                                                        mtObj.text = myObj.MessageBody;
                                                        mtObj.MessageBody = msg;
                                                        mtObj.MessageBody = resPlan.dupYouMsg;
                                                        let mtMsg = JSON.stringify(mtObj);
                                                        ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                                        // send sms duplicate you in old 1 !
                                                    } else {
                                                        var mtObj = new Object();
                                                        mtObj.From = myObj.To;
                                                        mtObj.To = myObj.From;
                                                        mtObj.status = 202;
                                                        mtObj.cat = cat;
                                                        mtObj.coll = coll;
                                                        mtObj.planId = resPlan.planId;
                                                        mtObj.row = 0;
                                                        mtObj.pinCode = pinCode;
                                                        mtObj.text = myObj.MessageBody;
                                                        mtObj.MessageBody = msg;
                                                        mtObj.MessageBody = resPlan.dupOthersMsg;
                                                        let mtMsg = JSON.stringify(mtObj);
                                                        ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                                        // send sms duplicate other in old 1 !
                                                    }
                                                }
                                            } else {
                                                let newCode = resPlan.row + 1;
                                                let msg = resPlan.resMsg;
                                                msg = msg.replace('[ROW]', newCode);
                                                db.collection(coll).updateOne(
                                                    {p: pinCode},
                                                    {$set: {v: 1, m: mobile, t: new Date(), l: resPlan.planId}}
                                                );
                                                db.collection('plans').updateOne(
                                                    {planId: resPlan.planId},
                                                    {$inc: {row: 1}}
                                                );
                                                var mtObj = new Object();
                                                mtObj.From = myObj.To;
                                                mtObj.To = myObj.From;
                                                mtObj.status = 200;
                                                mtObj.cat = cat;
                                                mtObj.coll = coll;
                                                mtObj.planId = resPlan.planId;
                                                mtObj.row = newCode;
                                                mtObj.pinCode = pinCode;
                                                mtObj.text = myObj.MessageBody;
                                                mtObj.MessageBody = msg;
                                                let mtMsg = JSON.stringify(mtObj);
                                                ch.publish('', mtq, new Buffer(mtMsg), {priority: 3});
                                                // send sms new row !
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                    setTimeout(function () {
                        if(ackFlag) {
                            ch.ack(msg);
                        }
                    },secs * 1000)}, {
                    noAck: false
                }
            );
        });
    });
});