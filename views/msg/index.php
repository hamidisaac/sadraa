<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MsgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'اینباکس 20004030';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'_id',
            //'ID',
            'From',
            //'To',
            'Date',
            'Body',
            'pin',
            'branchId',
            //'customerId',
            'plan',
            'status',
            'code',
            'msg',
            //'time',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
