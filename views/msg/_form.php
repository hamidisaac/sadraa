<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Msg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'From') ?>

    <?= $form->field($model, 'To') ?>

    <?= $form->field($model, 'Date') ?>

    <?= $form->field($model, 'Body') ?>

    <?= $form->field($model, 'pin') ?>

    <?= $form->field($model, 'branchId') ?>

    <?= $form->field($model, 'customerId') ?>

    <?= $form->field($model, 'plan') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'msg') ?>

    <?= $form->field($model, 'time') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
