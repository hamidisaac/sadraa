<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MsgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msg-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'From') ?>

    <?= $form->field($model, 'To') ?>

    <?= $form->field($model, 'Date') ?>

    <?= $form->field($model, 'pin') ?>

    <?php // echo $form->field($model, 'l') ?>

    <?php // echo $form->field($model, 'p') ?>

    <?php // echo $form->field($model, 'f') ?>

    <?php // echo $form->field($model, 'm') ?>

    <?php // echo $form->field($model, 'n') ?>

    <?php // echo $form->field($model, 'o') ?>

    <?php // echo $form->field($model, 'r') ?>

    <?php // echo $form->field($model, 't') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
