<div class="row">
    <div class="col-md-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">check_circle</i>
                </div>
                <p class="card-category">تعداد کل دریافت</p>
                <h3 class="card-title"><?=$all_count?></h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i>دوره جاری
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">check_circle</i>
                </div>
                <p class="card-category">تعداد کل ارسال</p>
                <h3 class="card-title"><?=$all_count?></h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i>دوره جاری
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">check_circle</i>
                </div>
                <p class="card-category">صحیح</p>
                <h3 class="card-title"><?=$s200?></h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i>دوره جاری
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">cancel</i>
                </div>
                <p class="card-category">ناصحیح</p>
                <h3 class="card-title"><?=$s201?></h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i>دوره جاری
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">control_point</i>
                </div>
                <p class="card-category">تکراری شماره متفاوت</p>
                <h3 class="card-title"><?=$s202?></h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i>دوره جاری
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">control_point_duplicate</i>
                </div>
                <p class="card-category">تکراری شماره مشابه</p>
                <h3 class="card-title"><?=$s204?></h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i>دوره جاری
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-success">
                <h4 class="card-title ">آمار روزانه</h4>
                <p class="card-category">جدول آمار روزانه تعداد ارسال و دریافت</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                        <th>
                            ردیف
                        </th>
                        <th>
                            تاریخ
                        </th>
                        <th>
                            تعداد
                        </th>
                        </thead>
                        <tbody>
                        <?php $i = 1;foreach($days as $day) {
                            echo '<tr><td>' . $i . '</td><td>' . $day['_id']['year'] . '/' . $day['_id']['month'] . '/' . $day['_id']['day'] . '</td><td class="text-primary">' . $day['count'] . '</td></tr>';
                            $i++;
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">آمار به تفکیک محصولات</h4>
                <p class="card-category">جدول آمار روزانه به تفکیک محصولات</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                        <th>
                            ردیف
                        </th>
                        <th>
                            محصول
                        </th>
                        <th>
                            تعداد
                        </th>
                        </thead>
                        <tbody>
                        <?php $i = 1;foreach($cats as $cat) {
                            $catName = '';
                            switch ($cat['_id']) {
                                case 0:
                                    $catName = 'نامشخص';
                                    break;
                                case 1:
                                    $catName = 'چای';
                                    break;
                                case 2:
                                    $catName = 'برنج';
                                    break;
                                case 3:
                                    $catName = 'بسته چاي';
                                    break;
                                case 4:
                                    $catName = 'فروشندگان برنج';
                                    break;
                                case 5:
                                    $catName = 'فروشندگان قند و شكر';
                                    break;
                                case 6:
                                    $catName = 'برنج ترنادو';
                                    break;
                                case 7:
                                    $catName = 'برنج جاودانه';
                                    break;
                                case 8:
                                    $catName = 'رفاه';
                                    break;
                                case 9:
                                    $catName = 'برنج دبي';
                                    break;
                                case 10:
                                    $catName = 'زیتون محسن';
                                    break;
                                case '11':
                                    $catName = 'برنج پاکستانی محسن';
                                    break;
                                case 12:
                                    $catName = 'برنج هندی محسن';
                                    break;
                                case 13:
                                    $catName = 'مرغ و گوشت';
                                    break;
                                case 14:
                                    $catName = 'حبوبات';
                                    break;
                                case 15:
                                    $catName = 'محصولات کارخانه ۱۵';
                                    break;
                                case 16:
                                    $catName = 'محصولات کارخانه ۱۶';
                                    break;
                                case 17:
                                    $catName = 'محصولات کارخانه ۱۷';
                                    break;
                                case 18:
                                    $catName = 'محصولات کارخانه گرمسار ۱۸';
                                    break;
                                default:
                                    $catName = $cat['_id'];
                            }
                            echo '<tr><td>' . $i . '</td><td>' . $catName . '</td><td class="text-primary">' . $cat['count'] . '</td></tr>';
                            $i++;
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card card-chart">
            <div class="card-header card-header-success">
                <div class="ct-chart" id="dailySalesChart"></div>
            </div>
            <div class="card-body">
                <h4 class="card-title">فروش روزانه</h4>
                <p class="card-category">
                    <span class="text-success">
                      <i class="fa fa-long-arrow-up"></i> 55% </span> رشد در فروش امروز.</p>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">access_time</i> ۴ دقیقه پیش
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
    <div class="card card-chart">
        <div class="card-header card-header-warning">
            <div class="ct-chart" id="websiteViewsChart"></div>
        </div>
        <div class="card-body">
            <h4 class="card-title">دنبال کننده‌های ایمیلی</h4>
            <p class="card-category">کارایی آخرین کمپین</p>
        </div>
        <div class="card-footer">
            <div class="stats">
                <i class="material-icons">access_time</i> کمپین دو روز پیش ارسال شد
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
    <div class="card card-chart">
        <div class="card-header card-header-danger">
            <div class="ct-chart" id="completedTasksChart"></div>
        </div>
        <div class="card-body">
            <h4 class="card-title">وظایف انجام شده</h4>
            <p class="card-category">کارایی آخرین کمپین</p>
        </div>
        <div class="card-footer">
            <div class="stats">
                <i class="material-icons">access_time</i> کمپین دو روز پیش ارسال شد
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-success">
                <h4 class="card-title">آمار کارکنان</h4>
                <p class="card-category">کارکنان جدید از ۱۵ آبان ۱۳۹۹</p>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead class="text-warning">
                    <th>کد</th>
                    <th>نام</th>
                    <th>حقوق</th>
                    <th>استان</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>احمد حسینی</td>
                        <td>$36,738</td>
                        <td>مازندران</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>مینا رضایی</td>
                        <td>$23,789</td>
                        <td>گلستان</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>مبینا احمدپور</td>
                        <td>$56,142</td>
                        <td>تهران</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>جلال آقایی</td>
                        <td>$38,735</td>
                        <td>شهرکرد</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h3 class="card-title">اعلان ها</h3>
            </div>
            <div class="card-body">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span>
                      این یک اعلان است که با کلاس "alert-danger" ایجاد شده است.</span>
                </div>
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span>
                      این یک اعلان است که با کلاس "alert-warning" ایجاد شده است.</span>
                </div>
                <div class="alert alert-primary">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span>
                      این یک اعلان است که با کلاس "alert-primary" ایجاد شده است.</span>
                </div>
                <div class="alert alert-info alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">add_alert</i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span data-notify="پیام">این یک اعلان با دکمه بستن و آیکن است</span>
                </div>
            </div>
        </div>
    </div>
</div>-->
