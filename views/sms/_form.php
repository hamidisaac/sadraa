<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sms-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 's') ?>

    <?= $form->field($model, 'd') ?>

    <?= $form->field($model, 'b') ?>

    <?= $form->field($model, 'c') ?>

    <?= $form->field($model, 'l') ?>

    <?= $form->field($model, 'p') ?>

    <?= $form->field($model, 'f') ?>

    <?= $form->field($model, 'm') ?>

    <?= $form->field($model, 'n') ?>

    <?= $form->field($model, 'o') ?>

    <?= $form->field($model, 'r') ?>

    <?= $form->field($model, 't') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
